#include <string.h>
#include <cpp-debug/Logger.hpp>
#include "Device.hpp"



Device::Device()
{
	memset(ID, 0, IDsize);
	memset(addr.IP, 0, IPsize);
}
Device::Device(const Device& other)
{
	strncpy(this->ID, other.ID, IDsize);
	strncpy(this->addr.IP, other.addr.IP, IPsize);
	this->addr.port = other.addr.port;
	this->deviceClass = other.deviceClass;
}
Device::~Device()
{
	
}

bool Device::Init(Device::Class deviceClass, const char* deviceID, const char* deviceIP, uint16_t devicePort)
{
	if(strlen(deviceID) >= IDsize)
	{
		LOG_WARNING("deviceID: " << deviceID << " is longer then available space in id field: " << IDsize);
		return false;
	}

	strncpy(this->ID, deviceID, IDsize);
	strncpy(this->addr.IP, deviceIP, IPsize);
	this->addr.port = devicePort;
	this->deviceClass = deviceClass;

	return true;
}

bool Device::IsID(const char* id) const
{
	return strncmp(this->ID, id, IDsize)==0;
}
Device::Addr& Device::GetAddr()
{
	return addr;
}

const char* FindMessaeTerminator(const char* data, int maxLength)
{
	int indents = 0;

	for(int i=0; i<maxLength; ++i)
	{
		if(data[i]=='\0')
			return 0;

		if(data[i]=='{')
		{
			indents++;
		}
		else if(data[i]=='}')
		{
			if(indents>0)
			{
				indents--;
			}
			else
			{
				return data+i;
			}
		}
	}

	return 0;
}

bool Device::ProcessMessage(const char* dataStart, int dataLength)
{
	const char* terminator = FindMessaeTerminator(dataStart, dataLength);
	const char* deveceStartStr = "deviceClass";
	const char* deviceIDStr = "deviceID";
	const char* valueStr = "value";

	if(terminator==0)
	{
		LOG_ERROR("Message to process by Device is empty");
		return false;
	}

	size_t dataPakcetLength = terminator - dataStart;

	std::string data(dataStart, dataPakcetLength);

	std::size_t deviceClassPos = data.find(deveceStartStr);
	std::size_t deviceIDPos = data.find(deviceIDStr);
	std::size_t valuePos = data.find(valueStr);

	if(deviceClassPos==std::string::npos ||
		deviceIDPos==std::string::npos ||
		valuePos==std::string::npos )
	{
		LOG_ERROR("Message to process is incomplete or foulty: {" << data << "}");
		return false;
	}

	deviceClassPos += strlen(deveceStartStr)+1;
	deviceIDPos += strlen(deviceIDStr)+1;
	valuePos += strlen(valueStr)+1;

	std::size_t deviceClassTerminator = data.find("}", deviceClassPos);
	std::size_t deviceIDTerminator = data.find("}", deviceIDPos);
	std::size_t valueTerminator = data.find("}", valuePos);

	std::size_t deviceClassLength = deviceClassTerminator - deviceClassPos;
	std::size_t deviceIDLength = deviceIDTerminator - deviceIDPos;
	std::size_t valueLength = valueTerminator - valuePos;

	std::string deviceClassValue(data, deviceClassPos, deviceClassLength);
	std::string deviceIDValue(data, deviceIDPos, deviceIDLength);
	std::string valueValue(data, valuePos, valueLength);

	std::string deviceClass;

	switch(this->deviceClass)
	{
	case Class::InputAnalog:
		deviceClass="IA";
		break;
	case Class::InputEvent:
		deviceClass="IE";
		break;
	case Class::OutputAnalog:
		deviceClass="OA";
		break;
	case Class::OuputEvent:
		deviceClass="OE";
		break;
	default:
		deviceClass = "Un";
		break;
	}

	if(deviceClass!=deviceClassValue)
		return false;

	if(deviceIDValue!=ID)
		return false;

	value = std::stof(valueValue);
	return true;
}