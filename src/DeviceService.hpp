#ifndef	_H_DEVICESERVICE
#define _H_DEVICESERVICE
#include <stdint.h>
#include <vector>
#include <cpp-debug/Time.hpp>
#include "Device.hpp"
#include "UdpService.hpp"

class DeviceService
{
	constexpr static const float keepAliveTimeout = 10.0;
	Debug::Time::Stamp keepAliveTimer;

	std::vector<Device::Addr> 	keepAliveAddrs;
	std::vector<Device> 		devices;


	Ipc::UdpService udpService;


	void KeepUdpChannelsAlive();
public:
	DeviceService();
	~DeviceService();
	

	void Tick();

	void RegisterDeviceType(Device* devidePrototype);
	void RemoveDeviceByIP(std::string IP);
};


#endif