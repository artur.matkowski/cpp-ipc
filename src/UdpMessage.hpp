#ifndef H_UdpMessageInfo
#define H_UdpMessageInfo
#include <ostream>
#include "Udp.hpp"


#define buffSize  2048
#define hostSize  16
#define ackTimeout  3.0 //sec 

namespace Ipc
{
	class UdpMessage
	{
	public:

		class OnComplete
		{
		public:
			virtual void OnMsgDelivered() = 0; 
			virtual void OnMsgFailed() = 0; 
		};

	private:
		uint16_t hash = 0;

		char 	host[hostSize];
		int 	msgSize = -1;
		int 	port;
		int8_t	ackRemain = 1;
		float	timer = 0.0;
		char 	msgBuff[buffSize];
		int 	propagationRetries = 3;
		OnComplete* onComplete = 0;
		

		UdpMessage & operator=(const UdpMessage&){ return *this; };
    	UdpMessage(const UdpMessage&){};

	public:
		UdpMessage();
		UdpMessage(UdpMessage&& other);
		~UdpMessage();

		void Init(const char* data, int size, const char* host, int port, OnComplete* onDeliveredAction = 0);
		void SetRetries(int i);

		bool IsHash(uint16_t hash);
		uint16_t GetHash();
		void DecreaseRemainAck();
		int8_t GetRemainAck();
		int GetRemainRetries();
		void HandleOnDeliveredCallback();
		void HandleOnFailedCallback();

		float GetTimeSinceLastActivity() const;
		void PropagationTick(float deltaTime, Udp& udp);

		UdpMessage& operator=(UdpMessage&& other);
	};
}

#endif