#include <cpp-debug/Logger.hpp>
#include "DeviceService.hpp"
#include "UdpService.hpp"

#define BROADCAST_ADDR "192.168.1.255"
#define BROADCAST_PORT 5005
#define BROADCAST_MSG "HomePiService"

DeviceService::DeviceService()
{
	udpService.Init(BROADCAST_PORT);
}
DeviceService::~DeviceService()
{
}

void DeviceService::Tick()
{
	
	KeepUdpChannelsAlive();
}

class PingDeliveredAction: public Ipc::UdpMessage::OnComplete
{
public:
	DeviceService* deviceSerive;
	std::string pingedIP;
	virtual void OnMsgDelivered() override 
	{

	}
	virtual void OnMsgFailed() override 
	{
		LOG_INFO("Removing device " << pingedIP << " due to timeout");
		deviceSerive->RemoveDeviceByIP(pingedIP);
	}
};

void DeviceService::KeepUdpChannelsAlive()
{
	Debug::Time::Stamp now = Debug::Time::Now();

	Debug::Time::Duration duration = now - keepAliveTimer;

	if(duration.count() >= keepAliveTimeout)
	{
		for (std::vector<Device::Addr>::iterator i = keepAliveAddrs.begin(); i != keepAliveAddrs.end(); ++i)
		{
			PingDeliveredAction* pda = new PingDeliveredAction();
			pda->deviceSerive = this;
			pda->pingedIP = i->IP;

			Ipc::UdpMessage msg;
			msg.Init("PING", strlen("PING"), i->IP, i->port, pda);
			msg.SetRetries(10);
				
			udpService.QueueMessageToSend(msg);
		}
		keepAliveTimer = Debug::Time::Now();
	}
}

void DeviceService::RemoveDeviceByIP(std::string IP)
{
	for(int i=keepAliveAddrs.size()-1; i>0; i--)
	{
		if(keepAliveAddrs[i].IP==IP)
		{
			keepAliveAddrs.erase(keepAliveAddrs.begin()+i);
		}
	}
}