#ifndef H_UdpService
#define H_UdpService
#include <vector>
#include "cpp-debug/Time.hpp"
#include "UdpMessage.hpp"
#include "Udp.hpp"

namespace Ipc
{
	class UdpService
	{
		std::vector<UdpMessage> v_outMessages;
		std::vector<UdpMessage> v_inMessages;
		Debug::Time::Stamp 		m_lastTickTime;

		Udp 					m_udpServer;
	
	public:
		UdpService();
		~UdpService();

		bool Init(int port);
		void Tick();
		void QueueMessageToSend(UdpMessage& msg);
		void BroadcastUdp(const char* data, int dataLength, const char* IP, uint16_t tgtPort);
		void InvalidateMessage(uint16_t hash);
		bool PopReceivedMessage(UdpMessage* out_msg);

	private:
		void HandleAckMsg(uint16_t hash);
		void HandleMsgPropagation(float deltaTime);
		void RemoveTimedOutMsgs();
		void HandleInMsg(const char* msgData, int msgSize, const char* host, int port);
	};
}

#endif