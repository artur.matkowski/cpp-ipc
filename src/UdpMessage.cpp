#include "UdpMessage.hpp"
#include "Udp.hpp"



uint16_t hash16 (const char* data, int size) 
{
	uint16_t h = 0;
	for(int i=0; i<size; ++i)
	{
		h = h*23131 + (unsigned char) (data[i]);
	}

	return (h & 0xffff);
}

namespace Ipc
{
	UdpMessage::UdpMessage()
	{}
	UdpMessage::UdpMessage(UdpMessage&& other)
	{
		*this = std::move(other);
	}
	UdpMessage::~UdpMessage()
	{
		if(onComplete!=0)
			delete onComplete;
	}

	void UdpMessage::Init(const char* data, int size, const char* host, int port, OnComplete* onDeliveredAction)
	{

		msgSize = size < buffSize ? size : buffSize;
		strncpy(msgBuff, data, msgSize);
		strncpy(this->host, host, hostSize);
		this->port = port;

		this->hash = hash16(msgBuff, msgSize);
		onComplete = onDeliveredAction;
	}
	void UdpMessage::SetRetries(int i)
	{
		propagationRetries = i;
	}

	bool UdpMessage::IsHash(uint16_t hash)
	{
		return this->hash==hash;
	}
	void UdpMessage::DecreaseRemainAck()
	{
		if( ackRemain > 0)
		{
			ackRemain--;
		}
	}
	int8_t UdpMessage::GetRemainAck()
	{
		return ackRemain;
	}
	uint16_t UdpMessage::GetHash()
	{
		return hash;
	}

	float UdpMessage::GetTimeSinceLastActivity() const
	{
		return timer;
	}

	void UdpMessage::HandleOnDeliveredCallback()
	{
		if(onComplete!=0)
		{
			onComplete->OnMsgDelivered();
		}
	}
	void UdpMessage::HandleOnFailedCallback()
	{
		if(onComplete!=0)
		{
			onComplete->OnMsgFailed();
		}
	}

	int UdpMessage::GetRemainRetries()
	{
		return propagationRetries;
	}

	void UdpMessage::PropagationTick(float deltaTime, Udp& udp)
	{
		timer -= deltaTime;

		if(timer <= 0.0)
		{
			timer = ackTimeout;
			propagationRetries -= 1;

			udp.WriteFromServerSocket(msgBuff, msgSize, host, port);
		}
	}
	UdpMessage& UdpMessage::operator=(UdpMessage&& other)
	{
		hash = other.hash;
		memcpy(host, other.host, hostSize);
		msgSize = other.msgSize;
		port = other.port;
		ackRemain = other.ackRemain;
		timer = other.timer;
		memcpy(msgBuff, other.msgBuff, buffSize);
		propagationRetries = other.propagationRetries;
		onComplete = other.onComplete;

		other.hash = 0;
		other.msgSize = 0;
		other.port = 0;
		other.ackRemain = 0;
		other.timer = 0.0;
		other.propagationRetries = 0;
		other.onComplete = 0;

		return *this;
	}

}