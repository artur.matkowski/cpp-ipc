#ifndef H_UDP
#define	H_UDP
#include <string.h> //memset
#include <stdlib.h> //exit(0);
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>


namespace Ipc{
	
	class Udp
	{
	private:
		int m_socket = -1; 
		int m_port = -1;


		struct sockaddr_in si_me;

	
	public:

		Udp(int Port = -1);
		~Udp();

		bool StartListening(int port);
		void StopListening();
		int GetPort();

		int Read(char* msgBuff, int* msgSize, char* remoteHost, int hostSize, bool isBlocking = false, uint16_t* remotePort = nullptr);
		static void Write(const char * buff, int buffsize, const char* host, int recvport, int sendport = -1, bool broadcast = false);
		void WriteFromServerSocket(const char * buff, int buffsize, const char* host, int recvport, bool broadcast = false);

	};
}




#endif