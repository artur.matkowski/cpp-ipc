#include "UdpService.hpp"
#include <cpp-debug/Logger.hpp>
#include <string.h>

namespace Ipc
{
	char *strnstr(const char *haystack, const char *needle, size_t len)
	{
		int i;
		size_t needle_len;

		if (0 == (needle_len = strnlen(needle, len)))
			return (char *)haystack;

		for (i=0; i<=(int)(len-needle_len); i++)
		{
			if ((haystack[0] == needle[0]) && (0 == strncmp(haystack, needle, needle_len)))
				return (char *)haystack;

			haystack++;
		}
		return NULL;
	}

	bool GetHashIfMsgIsAck(const char* msg, int msgsize,  uint16_t* hash)
	{
		char* ackstr = strnstr(msg, "ACK:", msgsize);

		unsigned int hash32;
		if(ackstr!=nullptr)
		{
			sscanf(ackstr, "ACK:%u", &hash32);
			*hash = (uint16_t)hash32;
			return true;
		}

		return false;
	}


	UdpService::UdpService()
	{
		m_lastTickTime = Debug::Time::Now();
	}

	UdpService::~UdpService()
	{}

	bool UdpService::Init(int port)
	{
		m_lastTickTime = Debug::Time::Now();
		v_outMessages.reserve(16);
		v_inMessages.reserve(16);

		return m_udpServer.StartListening(port);
	}

	void UdpService::Tick()
	{
		auto now = Debug::Time::Now();
		double deltaTime = (now - m_lastTickTime).count();
		int msgSize = buffSize;
		
		char msgData[buffSize];
		char host[hostSize];
		uint16_t port;
		int udpStatus;
		
		m_lastTickTime = now;
		

		do
		{
			msgSize = buffSize;

			udpStatus = m_udpServer.Read(msgData, &msgSize, host, hostSize, false, &port);

			if(udpStatus > 0)
			{
				uint16_t hash;
				bool isAck = GetHashIfMsgIsAck(msgData, msgSize, &hash);

				if(isAck)
				{
					HandleAckMsg(hash);
				}
				else
				{
					HandleInMsg(msgData, msgSize, host, port);
				}
			}	
		}while(udpStatus > 0);

		RemoveTimedOutMsgs();
		HandleMsgPropagation(deltaTime);
	}

	void UdpService::QueueMessageToSend(UdpMessage& msg)
	{
		v_outMessages.emplace_back(std::move(msg));
	}
	
	void UdpService::BroadcastUdp(const char* data, int dataLength, const char* tgtIP, uint16_t tgtPort)
	{
		m_udpServer.WriteFromServerSocket(data, dataLength, tgtIP, tgtPort, true);
	}

	void UdpService::HandleAckMsg(uint16_t hash)
	{
		for(int i=0; i<v_outMessages.size(); i++)
		{
			if(!v_outMessages[i].IsHash(hash))
			{
				continue;
			}
			
			v_outMessages[i].DecreaseRemainAck();
			
			uint8_t ackRemain = v_outMessages[i].GetRemainAck();

			if(ackRemain==0)
			{
				v_outMessages[i].HandleOnDeliveredCallback();
				v_outMessages.erase(v_outMessages.begin() + i);
			}
		}
	}

	void UdpService::HandleMsgPropagation(float deltaTime)
	{
		for(int i=0; i<v_outMessages.size(); i++)
		{
			v_outMessages[i].PropagationTick(deltaTime, m_udpServer);
		}
	}

	void UdpService::RemoveTimedOutMsgs()
	{
		for(int i=0; i<v_outMessages.size(); i++)
		{
			if(v_outMessages[i].GetRemainRetries() == 0)
			{
				v_outMessages[i].HandleOnFailedCallback();
				v_outMessages.erase(v_outMessages.begin() + i);
			}
		}
	}

	void UdpService::HandleInMsg(const char* msgData, int msgSize, const char* host, int port)
	{
		UdpMessage inmsg;
		const int ackMsgSize = 16;
		inmsg.Init(msgData, msgSize, host, port);
		v_inMessages.emplace_back(std::move(inmsg));

		char ackBuff[ackMsgSize];
		snprintf(ackBuff, ackMsgSize, "ACK:%u", inmsg.GetHash());
		m_udpServer.Write(ackBuff, ackMsgSize, host, port);
	}
	void UdpService::InvalidateMessage(uint16_t hash)
	{
		for(int i=0; i<v_outMessages.size(); i++)
		{
			if(v_outMessages[i].GetHash() == hash)
			{
				v_outMessages.erase(v_outMessages.begin() + i);
				return;
			}
		}
	}
	bool UdpService::PopReceivedMessage(UdpMessage* msg)
	{
		if(v_inMessages.size()>0)
		{
			*msg = std::move(v_inMessages[0]);
			v_inMessages.erase(v_inMessages.begin());
			return true;
		}
		return false;

	}
}