#include <algorithm>
#include <poll.h>
#include <cpp-debug/Logger.hpp>
#include "Udp.hpp"


namespace Ipc{

	Udp::Udp(int Port)
	{
		if(Port>0)
		{
			StartListening(Port);
		}
	}
	Udp::~Udp()
	{
		if( m_socket!=-1 )
	    	close( m_socket );
	}

	bool Udp::StartListening(int port)
	{
		m_port = port;

		if ((m_socket=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
		{
			LOG_ERROR( "Creating socket" );
			return false;
		}

		// zero out the structure
		memset((char *) &si_me, 0, sizeof(si_me));

		si_me.sin_family = AF_INET;
		si_me.sin_port = htons(m_port);
		si_me.sin_addr.s_addr = htonl(INADDR_ANY);

		//bind socket to port
		if( bind(m_socket , (struct sockaddr*)&si_me, sizeof(si_me) ) == -1)
		{
			LOG_ERROR( "Binding socket" );
			return false;
		}
		LOG_INFO( "Listening for Udp on port " << m_port );

		return true;
	}

	void Udp::StopListening()
	{
	    close( m_socket );
	    m_socket = -1;
	}

	int Udp::Read(char* outBuff, int* buffSize, char* remoteHost, int hostBuffSize, bool isBlocking, uint16_t* remotePort )
	{
		if( m_socket == -1 )
		{
			return -1;
		}
	    else if(!isBlocking)
	    {
		    struct pollfd pollStruct;
		    pollStruct.fd = m_socket;
	        pollStruct.events = POLLIN;
	        pollStruct.revents = 0;

	        int rv = poll( &pollStruct, 1, 0 ); 

	        if(rv<1)
	        {
	        	return 0;
	        }	    	
	    }

		struct sockaddr_in si_other;


		unsigned int slen = sizeof(sockaddr_in);

	    //try to receive some data, this is a blocking call
	    *buffSize = recvfrom(m_socket, outBuff, *buffSize, 0, (struct sockaddr *) &si_other, &slen);
		if( *buffSize == -1)
		{
			LOG_ERROR( "recvfrom" );
			return -2;
		}
		strncpy(remoteHost, inet_ntoa(si_other.sin_addr), hostBuffSize);
		
		if(remotePort!=nullptr)
		{
			*remotePort = ntohs(si_other.sin_port);
		}	


	    //print details of the client/peer and the data received
		LOG_DEBUG( "Received Udp msg: {" << outBuff << "} of size: " << *buffSize << ", from {"<<inet_ntoa(si_other.sin_addr)<<":"<<ntohs(si_other.sin_port)<<"}" );
	
		return 1;
	}

	void Udp::Write(const char * buff, int buffsize, const char* host, int port, int outport, bool broadcast)
	{
		struct sockaddr_in si_other;
	    int s;
	    unsigned int slen = sizeof(sockaddr_in);

	    //int buffLength = buff.size() + 1;
	    //char msg[buffsize] = {0};
	    //strcpy(msg, buff.c_str());


	    if ( (s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
	    {
			LOG_ERROR( "Creating socket" );
			exit(-1);
	    }

		if(broadcast)
		{
			int broadcastEnable = 1;
			int ret = setsockopt(s, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable));
		}

		struct sockaddr_in si_me;
		// zero out the structure
		memset((char *) &si_me, 0, sizeof(si_me));

		si_me.sin_family = AF_INET;
		si_me.sin_port = htons(outport);
		si_me.sin_addr.s_addr = htonl(INADDR_ANY);

		//bind socket to port
		if( outport>0 && bind(s , (struct sockaddr*)&si_me, sizeof(si_me) ) == -1)
		{
			LOG_ERROR( "Error: Binding socket to port: " << outport );
		}




	    memset((char *) &si_other, 0, sizeof(si_other));
	    si_other.sin_family = AF_INET;
	    si_other.sin_port = htons(port);

	    if (inet_aton(host , &si_other.sin_addr) == 0) 
	    {
			LOG_ERROR( "inet_aton() failed, host: " << host );
	        exit(-1);
	    }

	    LOG_DEBUG( "Sending Udp msg: {" << buff << "} of size: " << buffsize << " to {"<<host<<":"<<port<<"}" );

	    //send the message
	    if (sendto(s, buff, buffsize , 0 , (struct sockaddr *) &si_other, slen)==-1)
	    {
			LOG_ERROR( "sendto() Error while sending Udp msg: {" << buff << "} to {"<<host<<":"<<port<<"}" );
			exit(-1);
	    }

	    close(s);
	}


	void Udp::WriteFromServerSocket(const char * buff, int buffsize, const char* host, int port, bool broadcast)
	{
		struct sockaddr_in si_other;
	    unsigned int slen = sizeof(sockaddr_in);

	    memset((char *) &si_other, 0, sizeof(si_other));
	    si_other.sin_family = AF_INET;
	    si_other.sin_port = htons(port);

	    if (inet_aton(host , &si_other.sin_addr) == 0) 
	    {
			LOG_ERROR( "inet_aton() failed, host: " << host );
	        exit(-1);
	    }

	    LOG_DEBUG( "Sending Udp msg: {" << buff << "} of size: " << buffsize << " to {"<<host<<":"<<port<<"}" );

	    int broadcastEnable=1;
		int ret;

		if(broadcast)
			ret = setsockopt(m_socket, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable));


	    //send the message
	    if (sendto(m_socket, buff, buffsize , 0 , (struct sockaddr *) &si_other, slen)==-1)
	    {
			LOG_ERROR( "sendto() Error while sending Udp msg: {" << buff << "} to {"<<host<<":"<<port<<"}" );
			exit(-1);
	    }

	    if(broadcast)
	    {
		    broadcastEnable=0;
			ret=setsockopt(m_socket, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable));	
	    }
	}

	int Udp::GetPort()
	{
		return m_port;
	}

}