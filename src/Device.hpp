#ifndef _H_DEVICE
#define _H_DEVICE

#define IPsize 16
#define IDsize 16

class Device
{
public:
	struct Addr
	{
		char 				IP[IPsize];
		uint16_t 			port;
	};
	enum Class
	{
		Unknown = 0,
		InputAnalog = 1,
		InputEvent = 2,
		OutputAnalog = 3,
		OuputEvent = 4
	};
protected:
	char 	ID[IDsize];

	Class 	deviceClass;
	Addr	addr;
	float	value = 0.0;


public:
	Device();
	Device(const Device& other);
	virtual ~Device();
	
	bool Init(Class deviceClass, const char* deviceID, const char* deviceIP, uint16_t devicePort);
	bool IsID(const char* id) const;

	Device::Addr& GetAddr();

	bool ProcessMessage(const char* dataStart, int length);
};

#endif

