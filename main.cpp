#include <iostream>
#include <cpp-debug/Logger.hpp>
#include "UdpService.hpp"
#include "DeviceService.hpp"

//#define TESTS
int passed = 0;


void CheckResoult(const char* testname)
{
	if(passed!=1)
	{
		LOG_ERROR("Test " << testname << " failed");
		exit(-1);
	}
	else
	{
		LOG_INFO("Test " << testname << " passed");
	}
}

class TEST_BASE: public Ipc::UdpMessage::OnComplete
{
public:
};


class TestAVR: public TEST_BASE
{
public:
	virtual void OnMsgDelivered() override 
	{
		LOG_INFO("testavr OnMsgDelivered");
		passed += 1;
	}
	virtual void OnMsgFailed() override 
	{
		LOG_INFO("testavr OnMsgFailed");
		passed -= 1;
	}

	static void Run()
	{
		Ipc::UdpService udpservice;
		udpservice.Init(5005);

		Ipc::UdpMessage msg;
		msg.Init("testavr msg", strlen("testavr msg"), "192.168.1.20", 5005, new TestAVR());
			
		udpservice.QueueMessageToSend(msg);

		for(int i=10; i>0 && passed!=1; i--)
		{
			udpservice.Tick();
			sleep(1);
		}
	}
};

class Test1: public TEST_BASE
{

public:
	virtual void OnMsgDelivered() override 
	{
		LOG_INFO("Test1 OnMsgDelivered");
		passed += 1;
	}
	virtual void OnMsgFailed() override 
	{
		LOG_INFO("Test1 OnMsgFailed");
		passed -= 1;
	}

	void Run()
	{
		Ipc::UdpService udpservice;
		if( fork() == 0 ) // parent
		{
			LOG_DEBUG("In parrent process");

			udpservice.Init(5555);

			Ipc::UdpMessage msg;
			msg.Init("test1 msg", strlen("test1 msg"), "127.0.0.1", 5556, this);
				
			udpservice.QueueMessageToSend(msg);

			for(int i=10; i>0 && passed!=1; i--)
			{
				udpservice.Tick();
				sleep(1);
			}
		}
		else
		{
			LOG_DEBUG("In child process");
		
			udpservice.Init(5556);
			Ipc::UdpMessage msg;

			for(int i=10; i>0; i--)
			{
				udpservice.Tick();
				if(udpservice.PopReceivedMessage(&msg))
				{
					LOG_INFO("Popped msg int test1 receiver");
					exit(0);
				}
				sleep(1);
			}
			exit(0);
		}
	}
};



class Test2: public TEST_BASE
{

public:
	virtual void OnMsgDelivered() override 
	{
		LOG_INFO("Test2 OnMsgDelivered");
		passed -= 1;
	}
	virtual void OnMsgFailed() override 
	{
		LOG_INFO("Test2 OnMsgFailed");
		passed += 1;
	}

	void Run()
	{
		Ipc::UdpService udpservice;
		udpservice.Init(5555);

		Ipc::UdpMessage msg;
		msg.Init("test2 msg", strlen("test2 msg"), "127.0.0.1", 5556, this);
			
		udpservice.QueueMessageToSend(msg);

		for(int i=10; i>0 && passed!=1; i--)
		{
			udpservice.Tick();
			sleep(1);
		}
	}
};

int main(int argc, char** argv)
{
	Debug::Log::Init(std::cout, Debug::Log::Level::Debug);
	
	//DeviceService deviceService;

	//while(1)
	//deviceService.Tick();

#ifdef TESTS
	LOG_INFO("-----test:1");
	{
		Test1 test1;
		test1.Run();
		test1.CheckResoult("test1");
	}
	LOG_INFO("-----test:2");
	{
		Test2 test2;
		test2.Run();
		test2.CheckResoult("test2");
	}


#else
	LOG_INFO("-----test:AVR");
	{
		TestAVR testAVR;
		TestAVR::Run();
		CheckResoult("testAVR");
	}

#endif
	LOG_WARNING("TESTS PASSED");
	return 0;
} 
