#!/bin/bash



if [[ -z $(git status -s) ]]
then
  buildnr='build:'$(git rev-list --count HEAD)
else
  buildnr='build:'$(git rev-list --count HEAD)' wip on: '$(git rev-parse --abbrev-ref HEAD)
fi

apiVersion=$(git log --oneline | grep -w api-break: | head -n1 | cut -d " " -f1)
#echo $apiVersion

if [[ $apiVersion -eq "" ]]; then
  apiVersion=$(git rev-list --max-parents=0 HEAD)
fi


echo $(git log --oneline | grep -ow api-break: | wc -l).$(git log --oneline $apiVersion..HEAD | grep -ow release: | wc -l) $buildnr